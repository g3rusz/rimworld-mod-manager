# Rimworld Mod Manager

A little utility for managing Rimworld mods without waiting for a half hour for the game to start, for obsessive modders like me.