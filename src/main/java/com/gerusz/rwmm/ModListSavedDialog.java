package com.gerusz.rwmm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModListSavedDialog extends JDialog {
	private JPanel contentPane;
	private JButton buttonOK;
	private JLabel backupLocationLabel;

	public ModListSavedDialog(String backupPath) {
		backupLocationLabel.setText(backupPath);
		setContentPane(contentPane);
		setModal(false);
		getRootPane().setDefaultButton(buttonOK);

		buttonOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {onOK();}
		});
	}

	{
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
		$$$setupUI$$$();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		contentPane = new JPanel();
		contentPane.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel1,
		                new com.intellij.uiDesigner.core.GridConstraints(1,
		                                                                 0,
		                                                                 1,
		                                                                 1,
		                                                                 com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
		                                                                 com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH,
		                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
		                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
		                                                                 1,
		                                                                 null,
		                                                                 null,
		                                                                 null,
		                                                                 0,
		                                                                 false));
		buttonOK = new JButton();
		buttonOK.setText("OK");
		panel1.add(buttonOK,
		           new com.intellij.uiDesigner.core.GridConstraints(0,
		                                                            0,
		                                                            1,
		                                                            1,
		                                                            com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
		                                                            com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
		                                                            com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
		                                                            com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
		                                                            com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
		                                                            null,
		                                                            null,
		                                                            null,
		                                                            0,
		                                                            false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel2,
		                new com.intellij.uiDesigner.core.GridConstraints(0,
		                                                                 0,
		                                                                 1,
		                                                                 1,
		                                                                 com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
		                                                                 com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH,
		                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
		                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
		                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
		                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
		                                                                 null,
		                                                                 null,
		                                                                 null,
		                                                                 0,
		                                                                 false));
		backupLocationLabel = new JLabel();
		backupLocationLabel.setText("Label");
		panel2.add(backupLocationLabel,
		           new com.intellij.uiDesigner.core.GridConstraints(1,
		                                                            0,
		                                                            1,
		                                                            1,
		                                                            com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
		                                                            com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
		                                                            com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
		                                                            com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
		                                                            null,
		                                                            null,
		                                                            null,
		                                                            0,
		                                                            false));
		final JLabel label1 = new JLabel();
		label1.setText("Mod list successfully saved! Your old mod list was backed up to:");
		panel2.add(label1,
		           new com.intellij.uiDesigner.core.GridConstraints(0,
		                                                            0,
		                                                            1,
		                                                            1,
		                                                            com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
		                                                            com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
		                                                            com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
		                                                            com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
		                                                            null,
		                                                            null,
		                                                            null,
		                                                            0,
		                                                            false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() { return contentPane; }

	private void onOK() {
		// add your code here
		dispose();
	}
}
