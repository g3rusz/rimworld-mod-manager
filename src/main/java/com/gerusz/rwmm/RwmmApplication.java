package com.gerusz.rwmm;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.gerusz.rwmm.data.ActiveModList;
import com.gerusz.rwmm.data.ModDatabase;
import com.gerusz.rwmm.data.RimworldMod;
import com.gerusz.rwmm.guilogic.ModManagerFormLogic;
import com.gerusz.rwmm.guiutils.GuiUtils;
import com.gerusz.rwmm.utils.ModListLoader;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinReg;
import lombok.extern.java.Log;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Log
public class RwmmApplication {

	private static final XmlMapper mapper = new XmlMapper();
	public static ActiveModList activeModList;
	public static String gameVersion;
	public static ModManagerConfig modManagerConfig;
	private static ModManagerForm modManagerForm;

	public static boolean detectConfig() {
		// If Windows - get the install directory from the registry
		ModManagerConfig config = null;
		if (SystemUtils.IS_OS_WINDOWS) {
			config = detectWindowsConfig();
			if (config == null) {
				return false;
			}
		}
		else if (SystemUtils.IS_OS_LINUX) {
			config = detectLinuxConfig();
			if (config == null) {
				return false;
			}
		}
		else if (SystemUtils.IS_OS_MAC_OSX) {
			config = detectMacConfig();
			if (config == null) {
				return false;
			}
		}
		else {
			log.severe("Unknown OS");
			return false;
		}

		try {
			mapper.setDefaultUseWrapper(true);
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			/*String asString = mapper.writeValueAsString(config);
			System.out.println("What would be written to the new config is...");
			System.out.println(asString);*/
			mapper.writeValue(new File("config.xml"), config);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true; //TODO

	}

	public static void main(String... args) {
		modManagerConfig = loadConfig();
		log.info("Mod list: " + modManagerConfig.getModListPath());
		log.info("Internal mods: " + modManagerConfig.getInternalModsFolder());
		log.info("Steam mods: " + modManagerConfig.getSteamModsFolder());

		activeModList = ModListLoader.loadFromModList();
		log.info("Mods in active mod list: " + activeModList.activeMods.size());
		gameVersion = activeModList.version;

		ModDatabase.loadMods(modManagerConfig, activeModList);

		System.out.println("Active mods:");
		ModDatabase.queryValues(new ModDatabase.ModDbQuery().active(true)).stream()
		           .sorted(Comparator.comparing(RimworldMod::getName))
		           .forEachOrdered(entry -> System.out.println(String.format("%s\t\t-> %s (%s, %s)",
		                                                                     entry.getFolderName(),
		                                                                     entry.getName(),
		                                                                     entry.getModType(),
		                                                                     String.join(", ", entry.getVersions()))));

		System.out.println("Inactive mods (compatible):");
		final List<RimworldMod> inactiveCompatibleMods = ModDatabase.queryValues(new ModDatabase.ModDbQuery().active(false).gameVersion(activeModList.version));
		inactiveCompatibleMods.stream()
		                      .sorted(Comparator.comparing(RimworldMod::getName))
		                      .forEachOrdered(entry -> System.out.println(String.format("%s\t\t-> %s (%s, %s)",
		                                                                                entry.getFolderName(),
		                                                                                entry.getName(),
		                                                                                entry.getModType(),
		                                                                                String.join(", ", entry.getVersions()))));

		System.out.println("Inactive mods (incompatible):");
		ModDatabase.queryValues(new ModDatabase.ModDbQuery().active(false)).stream()
		           .filter(mod -> !inactiveCompatibleMods.contains(mod))
		           .sorted(Comparator.comparing(RimworldMod::getName))
		           .forEachOrdered(entry -> System.out.println(String.format("%s\t\t-> %s (%s, %s)",
		                                                                     entry.getFolderName(),
		                                                                     entry.getName(),
		                                                                     entry.getModType(),
		                                                                     String.join(", ", entry.getVersions()))));


		modManagerForm = new ModManagerForm();
		ModManagerFormLogic.initialize(modManagerForm);
		JFrame frame = new JFrame("Rimworld Mod Manager v0.0");
		frame.setContentPane(modManagerForm.getMainPanel());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}

	public static void saveModList(List<RimworldMod> activeMods) {
		ActiveModList changedList = new ActiveModList();
		changedList.version = gameVersion;
		changedList.activeMods = activeMods.stream().map(RimworldMod::getFolderName).collect(Collectors.toList());

		try {
			SimpleDateFormat backupDateFormat = new SimpleDateFormat("YYYY-MM-dd_HH-mm-ss");

			String backupFileName = "ModsConfig_" + backupDateFormat.format(new Date()) + ".xml";
			File modListFile = new File(modManagerConfig.getModListPath());
			File backupFile = new File(modManagerConfig.getModListPath().replace("ModsConfig.xml", backupFileName));
			FileUtils.copyFile(modListFile, backupFile);

			mapper.setDefaultUseWrapper(true);
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
			mapper.writeValue(modListFile, changedList);

			ModListSavedDialog savedDialog = new ModListSavedDialog(backupFile.getAbsolutePath());
			savedDialog.pack();
			GuiUtils.centerIn(savedDialog, modManagerForm.getMainPanel());
			savedDialog.setVisible(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static ModManagerConfig detectLinuxConfig() {
		String steamInstallPath = "~/.steam/steam/steamapps/common/Rimworld";
		if (new File(steamInstallPath).exists()) {
			String internalModsFolder = steamInstallPath + "/Mods";
			String steamModsFolder = steamInstallPath.replace("/common/Rimworld", "/workshop/content/294100");
			String modsListLocation = ".config/unity3d/Ludeon Studios/RimWorld/Config/ModsConfig.xml";
			String savegameFolder = ".config/unity3d/Ludeon Studios/RimWorld/Saves";
			// Check if these exist
			if (!new File(modsListLocation).exists()) {
				log.severe("Mod list file does not exist!");
				return null;
			}
			// Internal mods should always exist
			if (!new File(internalModsFolder).exists()) {
				log.severe("Internal mods folder not found!");
				return null;
			}
			// So should the savegame folder
			if (!new File(savegameFolder).exists()) {
				log.severe("Savegame folder not found!");
				return null;
			}
			// Steam mods folder might not exist if the user hasn't downloaded anything from the Workshop...
			if (!new File(steamModsFolder).exists()) {
				log.warning("Steam mods folder doesn't exist.");
				steamModsFolder = null;
			}
			return new ModManagerConfig(internalModsFolder, modsListLocation, savegameFolder, steamModsFolder);
		}
		else {
			return null;
		}
	}

	private static ModManagerConfig detectMacConfig() {
		String steamInstallPath = "~/Library/Application Support/Steam/steamapps/common/Rimworld";
		if (new File(steamInstallPath).exists()) {
			String internalModsFolder = steamInstallPath + "/Mods";
			String steamModsFolder = steamInstallPath.replace("/common/Rimworld", "/workshop/content/294100");
			String modsListLocation = "~/Library/Application Support/RimWorld/Config/ModsConfig.xml";
			String savegameFolder = "~/Library/Application Support/RimWorld/Saves";
			// Check if these exist
			if (!new File(modsListLocation).exists()) {
				log.severe("Mod list file does not exist!");
				return null;
			}
			// Internal mods should always exist
			if (!new File(internalModsFolder).exists()) {
				log.severe("Internal mods folder not found!");
				return null;
			}
			// So should the savegame folder
			if (!new File(savegameFolder).exists()) {
				log.severe("Savegame folder not found!");
				return null;
			}
			// Steam mods folder might not exist if the user hasn't downloaded anything from the Workshop...
			if (!new File(steamModsFolder).exists()) {
				log.warning("Steam mods folder doesn't exist.");
				steamModsFolder = null;
			}
			return new ModManagerConfig(internalModsFolder, modsListLocation, savegameFolder, steamModsFolder);
		}
		else {
			return null;
		}
	}

	private static ModManagerConfig detectWindowsConfig() {
		String internalModsFolder;
		String steamModsFolder = null;
		String modListLocation;
		String savegameFolder;
		try {
			String installLocation;
			boolean isSteam;
			if (Advapi32Util.registryValueExists(WinReg.HKEY_LOCAL_MACHINE,
			                                     "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App 294100",
			                                     "InstallLocation", WinNT.KEY_WOW64_64KEY)) {
				installLocation =
						Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE,
						                                    "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App 294100",
						                                    "InstallLocation", WinNT.KEY_WOW64_64KEY);
				isSteam = true;
			}
			else if (Advapi32Util.registryValueExists(WinReg.HKEY_LOCAL_MACHINE,
			                                          "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Rimworld",
			                                          "InstallLocation")) {
				installLocation =
						Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE,
						                                    "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Rimworld",
						                                    "InstallLocation");
				isSteam = false;
			}
			else {
				log.severe("Huh.");
				return null;
			}
			if (installLocation != null) {
				internalModsFolder = installLocation + "\\Mods";
				if (isSteam) {
					steamModsFolder = installLocation.replace("\\common\\RimWorld", "\\workshop\\content\\294100");
				}
				String userHomePath = SystemUtils.getUserHome().getAbsolutePath();
				modListLocation = userHomePath + "\\AppData\\LocalLow\\Ludeon Studios\\RimWorld by Ludeon Studios\\Config\\ModsConfig.xml";
				if (!new File(modListLocation).exists()) {
					log.severe("Mod list at " + modListLocation + " does not exist! Aborting...");
					return null;
				}

				savegameFolder = userHomePath + "\\AppData\\LocalLow\\Ludeon Studios\\RimWorld by Ludeon Studios\\Saves";
				if (!new File(savegameFolder).exists()) {
					log.severe("Savegame folder at " + savegameFolder + " does not exist! Aborting...");
					return null;
				}
			}
			else {
				log.severe("Could not find install location in Registry!");
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.severe("Couldn't read registry settings!");
			return null;
		}
		return new ModManagerConfig(internalModsFolder, modListLocation, savegameFolder, steamModsFolder);
	}

	private static ModManagerConfig loadConfig() {
		File configFile = new File("config.xml");
		if (!configFile.exists()) {
			boolean configDetected = detectConfig();
			if (!configDetected) {
				log.severe("Could not find config file at " + configFile.getAbsolutePath() + ". Exiting...");
				System.exit(-1);
				return null;
			}
		}
		try {
			ModManagerConfig config = mapper.readValue(configFile, ModManagerConfig.class);
			if (config.getSavegameFolder() == null) {
				config.setSavegameFolder(config.getModListPath().replace("Config" + File.separator + "ModsConfig.xml", "Saves"));
				try {
					mapper.setDefaultUseWrapper(true);
					mapper.enable(SerializationFeature.INDENT_OUTPUT);
					mapper.writeValue(configFile, config);
				} catch (IOException e) {
					log.warning("Couldn't save config file with savegame folder!");
				}
			}
			return config;
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-2);
			return null;
		}
	}
}
