package com.gerusz.rwmm.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AboutFileContents {
	public String name;
	public List<String> supportedVersions;
	public String targetVersion;
	public String author;
	public String url;
	public String description;
}
