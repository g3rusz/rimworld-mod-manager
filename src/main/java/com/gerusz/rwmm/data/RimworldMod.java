package com.gerusz.rwmm.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RimworldMod {
	private boolean active;
	private String folderName;
	private ModType modType;
	private String name;
	private String[] versions;
	public enum ModType {
		CORE,
		INTERNAL,
		STEAM
	}
	AboutFileContents aboutFileContents;
}
