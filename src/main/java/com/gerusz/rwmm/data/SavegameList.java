package com.gerusz.rwmm.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class SavegameList {
	@Getter
	@Setter
	@AllArgsConstructor
	public static class Savegame {
		private String fileName;
		private String gameVersion;
		private List<String> modFolders;
		private List<String> modNames;
	}
}
