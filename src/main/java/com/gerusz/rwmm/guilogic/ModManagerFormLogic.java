package com.gerusz.rwmm.guilogic;

import com.gerusz.rwmm.ModDetailsDialog;
import com.gerusz.rwmm.ModManagerForm;
import com.gerusz.rwmm.RwmmApplication;
import com.gerusz.rwmm.data.ModDatabase;
import com.gerusz.rwmm.data.RimworldMod;
import com.gerusz.rwmm.guiutils.GuiUtils;
import com.gerusz.rwmm.guiutils.ModTableModel;
import com.gerusz.rwmm.utils.ModVersionMatcher;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static com.gerusz.rwmm.data.RimworldMod.ModType.*;

@Log
public class ModManagerFormLogic {

	private static ModDetailsDialog modDetailsDialog;

	public static void initialize(final ModManagerForm modManagerForm) {
		fillActiveModsTable(modManagerForm);
		fillInactiveModsTable(modManagerForm);

		modManagerForm.getMainPanel().addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				setDefaultColumnWidths(modManagerForm.getActiveModsTable());
				setDefaultColumnWidths(modManagerForm.getInactiveModsTable());
			}
		});

		modManagerForm.getActivateModsButton().addActionListener(e -> activateMods(modManagerForm));
		modManagerForm.getDeactivateButton().addActionListener(e1 -> deactivateMods(modManagerForm));

		modManagerForm.getTopButton().addActionListener(actionEvent2 -> moveModsToTop(modManagerForm));
		modManagerForm.getCompactUpButton().addActionListener(actionEvent1 -> compactModsUp(modManagerForm));
		modManagerForm.getUpButton().addActionListener(actionEvent2 -> moveModsUp(modManagerForm));
		modManagerForm.getDownButton().addActionListener(actionEvent1 -> moveModsDown(modManagerForm));
		modManagerForm.getCompactDownButton().addActionListener(actionEvent -> compactModsDown(modManagerForm));
		modManagerForm.getBottomButton().addActionListener(actionEvent -> moveModsToBottom(modManagerForm));

		modManagerForm.getShowIncompatibleCheckBox().addItemListener(itemEvent -> showIncompatibleValueChanged(modManagerForm));

		modManagerForm.getActiveModsHighlighterField().getDocument().addDocumentListener(new HighlighterListener(true, modManagerForm));
		modManagerForm.getInactiveModsHighlighterField().getDocument().addDocumentListener(new HighlighterListener(false, modManagerForm));

		modManagerForm.getExitButton().addActionListener(e -> System.exit(0));
		modManagerForm.getSaveButton().addActionListener(e -> RwmmApplication.saveModList(modManagerForm.getActiveModsList()));

		modManagerForm.getActiveModsTable().setDefaultRenderer(String.class, new HighlighterRenderer(modManagerForm));
		modManagerForm.getInactiveModsTable().setDefaultRenderer(String.class, new HighlighterRenderer(modManagerForm));

		modManagerForm.getActiveModsTable().addMouseListener(new DoubleClickListener(modManagerForm));
		modManagerForm.getInactiveModsTable().addMouseListener(new DoubleClickListener(modManagerForm));
	}

	private static void activateMods(ModManagerForm modManagerForm) {
		int[] inactiveModsSelected = modManagerForm.getInactiveModsTable().getSelectedRows();
		List<RimworldMod> toActivate =
				Arrays.stream(inactiveModsSelected)
				      .mapToObj(
						      index -> getModAt(
								      index,
								      modManagerForm.getInactiveModsTable(),
								      modManagerForm.getInactiveModsList()))
				      .collect(Collectors.toList());
		modManagerForm.getInactiveModsList().removeAll(toActivate);
		modManagerForm.getActiveModsList().addAll(toActivate);
		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
	}

	private static void compactModsDown(ModManagerForm modManagerForm) {
		List<RimworldMod> selectedMods = getSelectedActiveMods(modManagerForm);
		// The active mod list ALWAYS begins with core. Always. So if it is selected, it's the first one.
		if (selectedMods.get(0).getModType() == CORE) {
			selectedMods.remove(0);
		}

		Collections.reverse(selectedMods);

		int lastModIndex = modManagerForm.getActiveModsList().indexOf(selectedMods.get(0));

		// Compact the rest just above the last one
		for (int i = 1; i < selectedMods.size(); i++) {
			RimworldMod mod = selectedMods.get(i);
			modManagerForm.getActiveModsList().remove(mod);
			modManagerForm.getActiveModsList().add(lastModIndex - i, mod);
		}

		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
		reselectModsInActive(modManagerForm.getActiveModsList(), modManagerForm.getActiveModsTable(), selectedMods);
	}

	private static void compactModsUp(ModManagerForm modManagerForm) {
		List<RimworldMod> selectedMods = getSelectedActiveMods(modManagerForm);
		// For a change, we don't remove Core since the first mod will never be moved
		int firstModIndex = modManagerForm.getActiveModsList().indexOf(selectedMods.get(0));

		// Compact the rest just under the first one
		for (int i = 1; i < selectedMods.size(); i++) {
			RimworldMod mod = selectedMods.get(i);
			modManagerForm.getActiveModsList().remove(mod);
			modManagerForm.getActiveModsList().add(firstModIndex + i, mod);
		}

		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
		reselectModsInActive(modManagerForm.getActiveModsList(), modManagerForm.getActiveModsTable(), selectedMods);
	}

	private static void deactivateMods(ModManagerForm modManagerForm) {
		List<RimworldMod> toDeactivate = getSelectedActiveMods(modManagerForm);
		modManagerForm.getActiveModsList().removeAll(toDeactivate);
		modManagerForm.getInactiveModsList().addAll(toDeactivate);
		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
	}

	private static void fillActiveModsTable(final ModManagerForm modManagerForm) {

		Map<String, RimworldMod> activeMods = ModDatabase.queryMap(new ModDatabase.ModDbQuery().active(true));
		modManagerForm.getActiveModsTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		//activeModsTable.setTableHeader(getTableHeader(aTcId, aNcId, aVcId));

		for (String modFolder : RwmmApplication.activeModList.activeMods) {
			RimworldMod mod = activeMods.get(modFolder);
			if (mod != null) {
				modManagerForm.getActiveModsList().add(mod);
			}
		}
		TableModel tableModel = new ModTableModel(modManagerForm.getActiveModsList());
		modManagerForm.getActiveModsTable().setModel(tableModel);
		setDefaultColumnWidths(modManagerForm.getActiveModsTable());
		modManagerForm.getActiveModsTable().addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				setDefaultColumnWidths(modManagerForm.getActiveModsTable());
			}

			@Override
			public void componentShown(ComponentEvent e) {
				setDefaultColumnWidths(modManagerForm.getActiveModsTable());
			}
		});

		modManagerForm.getActiveModsTable().getSelectionModel().addListSelectionListener(e -> setActiveModsPanelButtonsActiveState(modManagerForm));
	}

	private static void fillInactiveModsTable(final ModManagerForm modManagerForm) {

		List<RimworldMod> inactiveMods = ModDatabase.queryValues(new ModDatabase.ModDbQuery().active(false));
		ModDatabase.ModDbQuery versionFilter = new ModDatabase.ModDbQuery().gameVersion(RwmmApplication.gameVersion);
		List<RimworldMod>
				inactiveCompatible =
				inactiveMods.stream().filter(versionFilter::matches)
				            .filter(mod -> !modManagerForm.getActiveModsList()
				                                          .contains(mod)) // This method is also called when the incompatible / compatible filter is toggled
				            .sorted(Comparator.comparing(RimworldMod::getName)).collect(Collectors.toList());
		List<RimworldMod>
				inactiveIncompatible =
				inactiveMods.stream()
				            .filter(mod -> !versionFilter.matches(mod))
				            .filter(mod -> !modManagerForm.getActiveModsList().contains(mod))
				            .sorted(Comparator.comparing(RimworldMod::getName))
				            .collect(Collectors.toList());

		modManagerForm.getInactiveModsTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		modManagerForm.getInactiveModsList().clear();
		modManagerForm.getInactiveModsList().addAll(inactiveCompatible);
		if (modManagerForm.getShowIncompatibleCheckBox().isSelected()) {
			modManagerForm.getInactiveModsList().addAll(inactiveIncompatible);
		}

		modManagerForm.getInactiveModsTable().setModel(new ModTableModel(modManagerForm.getInactiveModsList()));
		setDefaultColumnWidths(modManagerForm.getInactiveModsTable());
		modManagerForm.getInactiveModsTable().addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				setDefaultColumnWidths(modManagerForm.getInactiveModsTable());
			}

			@Override
			public void componentShown(ComponentEvent e) {
				setDefaultColumnWidths(modManagerForm.getInactiveModsTable());
			}
		});

		modManagerForm.getInactiveModsTable().getSelectionModel().addListSelectionListener(e -> setActivateButtonActiveState(modManagerForm));
	}

	private static RimworldMod getModAt(int selection, JTable table, List<RimworldMod> modsInTable) {
		String modName = (String) table.getValueAt(selection, 1);
		String[] modVersions = ((String) table.getValueAt(selection, 2)).split(", ");
		Object modTypeObject = table.getValueAt(selection, 0);
		RimworldMod.ModType
				modType =
				Objects.equals(modTypeObject, ModTableModel.coreIcon) ? CORE : Objects.equals(modTypeObject, ModTableModel.steamIcon) ? STEAM : INTERNAL;

		List<RimworldMod> candidates = modsInTable.stream().filter(mod -> mod.getName().equals(modName)).collect(Collectors.toList());
		if (candidates.size() == 1) return candidates.get(0);
		candidates = candidates.stream().filter(mod -> mod.getModType() == modType).collect(Collectors.toList());
		if (candidates.size() == 1) return candidates.get(0);
		candidates = candidates.stream().filter(mod -> Arrays.equals(mod.getVersions(), modVersions)).collect(Collectors.toList());
		if (candidates.size() == 1) return candidates.get(0);
		else if (candidates.size() > 1) {
			log.warning("Somehow more than one mods matched the same type-name-version triple. Curious...");
			return candidates.get(0);
		}
		else {
			log.warning("No mods matched the value in the table. Curiouser...");
		}
		return null;
	}

	private static List<RimworldMod> getSelectedActiveMods(ModManagerForm modManagerForm) {
		int[] activeModsSelected = modManagerForm.getActiveModsTable().getSelectedRows();
		return Arrays.stream(activeModsSelected).filter(x -> x > 0) // Active mods can't be sorted, hence this is always the core
		             .mapToObj(index -> getModAt(index, modManagerForm.getActiveModsTable(), modManagerForm.getActiveModsList()))
		             .collect(Collectors.toList());
	}

	private static void moveModsDown(ModManagerForm modManagerForm) {
		List<RimworldMod> selectedMods = getSelectedActiveMods(modManagerForm);
		// The active mod list ALWAYS begins with core. Always. So if it is selected, it's the first one.
		if (selectedMods.get(0).getModType() == CORE) {
			selectedMods.remove(0);
		}

		Collections.reverse(selectedMods);

		for (RimworldMod mod : selectedMods) {
			int modIndex = modManagerForm.getActiveModsList().indexOf(mod);
			if (modIndex > 0 && modIndex < modManagerForm.getActiveModsList().size() - 1) {
				RimworldMod modAtTarget = modManagerForm.getActiveModsList().get(modIndex + 1);
				if (!selectedMods.contains(modAtTarget)) {
					modManagerForm.getActiveModsList().remove(modIndex);
					modManagerForm.getActiveModsList().add(modIndex + 1, mod);
				}
			}
		}
		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
		reselectModsInActive(modManagerForm.getActiveModsList(), modManagerForm.getActiveModsTable(), selectedMods);
	}

	private static void moveModsToBottom(ModManagerForm modManagerForm) {
		List<RimworldMod> selectedMods = getSelectedActiveMods(modManagerForm);
		// The active mod list ALWAYS begins with core. Always. So if it is selected, it's the first one.
		if (selectedMods.get(0).getModType() == CORE) {
			selectedMods.remove(0);
		}

		// The easiest way of doing this: remove them and add them again.
		modManagerForm.getActiveModsList().removeAll(selectedMods);
		modManagerForm.getActiveModsList().addAll(selectedMods);
		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
		reselectModsInActive(modManagerForm.getActiveModsList(), modManagerForm.getActiveModsTable(), selectedMods);
	}

	private static void moveModsToTop(ModManagerForm modManagerForm) {
		List<RimworldMod> selectedMods = getSelectedActiveMods(modManagerForm);
		// The active mod list ALWAYS begins with core. Always. So if it is selected, it's the first one.
		if (selectedMods.get(0).getModType() == CORE) {
			selectedMods.remove(0);
		}

		// The easiest way: remove them all and reinsert them at 1.
		modManagerForm.getActiveModsList().removeAll(selectedMods);
		modManagerForm.getActiveModsList().addAll(1, selectedMods);
		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
		reselectModsInActive(modManagerForm.getActiveModsList(), modManagerForm.getActiveModsTable(), selectedMods);
	}

	private static void moveModsUp(ModManagerForm modManagerForm) {
		List<RimworldMod> selectedMods = getSelectedActiveMods(modManagerForm);
		// The active mod list ALWAYS begins with core. Always. So if it is selected, it's the first one.
		if (selectedMods.get(0).getModType() == CORE) {
			selectedMods.remove(0);
		}

		// Move them all up except if they are already the mod just below the core.
		for (RimworldMod mod : selectedMods) {
			int modIndex = modManagerForm.getActiveModsList().indexOf(mod);
			if (modIndex > 1) {
				RimworldMod modAtTarget = modManagerForm.getActiveModsList().get(modIndex - 1);
				if (!selectedMods.contains(modAtTarget)) { // This can happen if moving hits the top of the list
					modManagerForm.getActiveModsList().remove(modIndex);
					modManagerForm.getActiveModsList().add(modIndex - 1, mod);
				}
			}
		}
		redrawTables(modManagerForm.getActiveModsTable(), modManagerForm.getInactiveModsTable());
		reselectModsInActive(modManagerForm.getActiveModsList(), modManagerForm.getActiveModsTable(), selectedMods);
	}

	private static void redrawTables(JTable activeModsTable, JTable inactiveModsTable) {
		inactiveModsTable.tableChanged(new TableModelEvent(inactiveModsTable.getModel()));
		activeModsTable.tableChanged(new TableModelEvent(activeModsTable.getModel()));
	}

	private static void reselectModsInActive(List<RimworldMod> activeModsList, JTable activeModsTable, List<RimworldMod> selectedMods) {
		int[] indices = selectedMods.stream().mapToInt(activeModsList::indexOf).toArray();
		activeModsTable.clearSelection();
		for (int index : indices) {
			activeModsTable.changeSelection(index, 1, true, false);
		}
	}

	private static void resizeNameColumn(JTable table) {
		int totalWidth = table.getParent().getWidth();
		int versionColumnWidth = table.getColumnModel().getColumn(2).getWidth();
		int nameColumnWidth = totalWidth - 45 - versionColumnWidth;
		table.getColumnModel().getColumn(1).setWidth(nameColumnWidth);
		table.getColumnModel().getColumn(1).setPreferredWidth(nameColumnWidth);
		table.getColumnModel().getColumn(1).setResizable(true);
	}

	private static void setActivateButtonActiveState(ModManagerForm modManagerForm) {
		modManagerForm.getActivateModsButton().setEnabled(modManagerForm.getInactiveModsTable().getSelectedRows().length > 0);
	}

	private static void setActiveModsPanelButtonsActiveState(ModManagerForm modManagerForm) {
		int[] selectedRows = modManagerForm.getActiveModsTable().getSelectedRows();
		boolean onlyCoreSelected = selectedRows.length == 1 && selectedRows[0] == 0;
		// Never EVER let the user deactivate or move core. (Also deactivate the buttons if nothing is selected.)
		if (onlyCoreSelected || selectedRows.length == 0) {
			modManagerForm.getDeactivateButton().setEnabled(false);
			modManagerForm.getTopButton().setEnabled(false);
			modManagerForm.getCompactUpButton().setEnabled(false);
			modManagerForm.getUpButton().setEnabled(false);
			modManagerForm.getDownButton().setEnabled(false);
			modManagerForm.getCompactDownButton().setEnabled(false);
			modManagerForm.getBottomButton().setEnabled(false);
			return;
		}

		modManagerForm.getDeactivateButton().setEnabled(true);

		//Other special cases: only the first non-core mod(s) is(are) selected (up and top are not active),
		// only the last non-core mod(s) is(are) selected (down and bottom are not active)
		boolean onlyTopSelected = true;
		int coreBias = selectedRows[0] == 0 ? 0 : 1; // If the core is not selected, shifts the start of the possible contiguous interval.
		for (int i = 0; i < selectedRows.length && onlyTopSelected; i++) {
			onlyTopSelected = (selectedRows[i] == i + coreBias); // This will interrupt the cycle if the selection is non-contiguous from the top
		}

		boolean onlyBottomSelected = true;
		int rowCount = modManagerForm.getActiveModsTable().getRowCount();
		for (int i = 0; i < selectedRows.length && onlyBottomSelected; i++) {
			onlyBottomSelected = (selectedRows[selectedRows.length - i - 1] == rowCount - i - 1);
			// Yadda yadda, only from the bottom
		}

		boolean canCompact = false; // Compacting is only enabled if the mods don't already form a contiguous selection
		if (selectedRows.length > 1) {
			int firstSelectedIndex = selectedRows[0];
			for (int i = 1; i < selectedRows.length && !canCompact; i++) {
				canCompact = (selectedRows[i] != firstSelectedIndex + i);
			}
		}


		modManagerForm.getTopButton().setEnabled(!onlyTopSelected);
		modManagerForm.getCompactUpButton().setEnabled(canCompact);
		modManagerForm.getUpButton().setEnabled(!onlyTopSelected);
		modManagerForm.getDownButton().setEnabled(!onlyBottomSelected);
		modManagerForm.getCompactDownButton().setEnabled(canCompact);
		modManagerForm.getBottomButton().setEnabled(!onlyBottomSelected);
	}

	private static void setDefaultColumnWidths(JTable table) {
		// Type - 45, non-resizable.
		// Version - 75, resizable
		// Name - whatever is left
		int totalWidth = table.getParent().getWidth();
		table.getColumnModel().getColumn(0).setWidth(45);
		table.getColumnModel().getColumn(0).setPreferredWidth(45);
		table.getColumnModel().getColumn(0).setResizable(false);

		int versionColumnWidth = table.getColumnModel().getColumn(2).getWidth();
		if (versionColumnWidth <= 75) {
			table.getColumnModel().getColumn(2).setWidth(75);
			table.getColumnModel().getColumn(2).setPreferredWidth(75);
		}
		table.getColumnModel().getColumn(2).setMinWidth(75);
		table.getColumnModel().getColumn(2).setResizable(true);

		int nameColumnWidth = totalWidth - 45 - versionColumnWidth;
		table.getColumnModel().getColumn(1).setWidth(nameColumnWidth);
		table.getColumnModel().getColumn(1).setPreferredWidth(nameColumnWidth);
		table.getColumnModel().getColumn(1).setResizable(false);

		table.getColumnModel().getColumn(2).addPropertyChangeListener(evt -> {
			if (evt.getPropertyName().equals("preferredWidth")) {
				resizeNameColumn(table);
			}
		});
	}

	private static void showIncompatibleValueChanged(ModManagerForm modManagerForm) {
		fillInactiveModsTable(modManagerForm);
	}

	private static class DoubleClickListener extends MouseAdapter {
		private ModManagerForm modManagerForm;

		public DoubleClickListener(ModManagerForm modManagerForm) {this.modManagerForm = modManagerForm;}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() > 1) {
				JTable table = (JTable) e.getSource();
				Point point = e.getPoint();
				int row = table.rowAtPoint(point);
				List<RimworldMod>
						modList =
						Objects.equals(table, modManagerForm.getActiveModsTable()) ? modManagerForm.getActiveModsList() : modManagerForm.getInactiveModsList();
				RimworldMod clickedMod = getModAt(row, table, modList);
				if (clickedMod != null) {
					if (modDetailsDialog != null) {
						modDetailsDialog.dispose();
					}
					ModManagerFormLogic.modDetailsDialog = new ModDetailsDialog(SwingUtilities.getWindowAncestor(modManagerForm.getMainPanel()), clickedMod);
					modDetailsDialog.displayMod();
					modDetailsDialog.setVisible(true);
					modDetailsDialog.setModal(true);

					modDetailsDialog.pack();
					modDetailsDialog.scrollToTop();
					GuiUtils.centerIn(modDetailsDialog, modManagerForm.getMainPanel());
					//detailsDialog.repaint();
				}
			}
		}
	}

	@AllArgsConstructor
	private static class HighlighterListener implements DocumentListener {

		private boolean active;
		private ModManagerForm modManagerForm;

		@Override
		public void changedUpdate(DocumentEvent e) {
			String filterText = null;
			try {
				filterText = e.getDocument().getText(0, e.getDocument().getLength());
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			if (active) {
				modManagerForm.setActiveHighlightFilter(filterText);
				modManagerForm.getActiveModsTable().repaint();
			}
			else {
				modManagerForm.setInactiveHighlightFilter(filterText);
				modManagerForm.getInactiveModsTable().repaint();
			}
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			changedUpdate(e);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			changedUpdate(e);
		}
	}

	private static class HighlighterRenderer extends JLabel implements TableCellRenderer {


		private ModManagerForm modManagerForm;


		public HighlighterRenderer(ModManagerForm modManagerForm) {
			this.modManagerForm = modManagerForm;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			String filter = (Objects.equals(table, modManagerForm.getActiveModsTable()) ?
			                 modManagerForm.getActiveHighlightFilter() :
			                 modManagerForm.getInactiveHighlightFilter());

			Component cell = new DefaultTableCellRenderer().getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			// Check version
			boolean versionMatches = true;
			String versionsString = table.getValueAt(row, 2).toString();
			if (!ModVersionMatcher.matches(versionsString)) {
				cell.setForeground(Color.RED);
				versionMatches = false;
			}

			if (StringUtils.isEmpty(filter)) {
				return cell;
			}
			// Get the name value from the table
			String modName = table.getValueAt(row, 1).toString();
			if (!modName.toLowerCase().contains(filter.toLowerCase())) {
				return cell;
			}
			cell.setBackground(isSelected ? Color.BLUE : Color.CYAN);
			if (versionMatches) {
				cell.setForeground(isSelected ? Color.WHITE : Color.BLACK);
			}
			else {
				cell.setForeground(Color.MAGENTA);
			}
			return cell;
		}
	}
}
