package com.gerusz.rwmm.guiutils;

import java.awt.*;

public class GuiUtils {
	public static void centerIn(Component what, Component inWhat) {
		what.setLocation(inWhat.getLocationOnScreen().x + (inWhat.getWidth() - what.getWidth()) / 2,
		                 inWhat.getLocationOnScreen().y + (inWhat.getHeight() - what.getHeight()) / 2);
	}
}
