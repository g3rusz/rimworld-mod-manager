package com.gerusz.rwmm.guiutils;

import com.gerusz.rwmm.data.RimworldMod;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.io.IOException;
import java.util.List;

public class ModTableModel extends AbstractTableModel {

	public static final Object coreIcon;
	public static final Object internalIcon;
	public static final Object steamIcon;

	static {
		ImageIcon si;
		try {
			si = new ImageIcon(ImageIO.read(ModTableModel.class.getResource("/steam_favicon.bmp").openStream()));
		} catch (IOException e) {
			e.printStackTrace();
			si = null;
		}
		steamIcon = si != null ? si : new Object();
		ImageIcon ci;
		try {
			ci = new ImageIcon(ImageIO.read(ModTableModel.class.getResource("/core_icon.bmp").openStream()));
		} catch (IOException e) {
			e.printStackTrace();
			ci = null;
		}
		coreIcon = ci != null ? ci : new Object();
		ImageIcon ii;
		try {
			ii = new ImageIcon(ImageIO.read(ModTableModel.class.getResource("/internal_icon.bmp").openStream()));
		} catch (IOException e) {
			e.printStackTrace();
			ii = null;
		}
		internalIcon = ii != null ? ii : new Object();
	}

	public List<RimworldMod> mods;

	public ModTableModel(List<RimworldMod> modList) {
		this.mods = modList;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnIndex == 0 ? ImageIcon.class : String.class;
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
			case 0:
				return "Type";
			case 1:
				return "Name";
			case 2:
				return "Version(s)";
			default:
				return "";
		}
	}

	@Override
	public int getRowCount() {
		return mods.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= mods.size() || columnIndex > 2) return null;
		RimworldMod mod = mods.get(rowIndex);
		switch (columnIndex) {
			case 0:
				switch (mod.getModType()) {
					case CORE:
						return coreIcon;
					case STEAM:
						return steamIcon;
					case INTERNAL:
						return internalIcon;
					default:
						return "";
				}
			case 1:
				return mod.getName();
			case 2:
				return String.join(", ", mod.getVersions());
			default:
				return null;
		}
	}
}
