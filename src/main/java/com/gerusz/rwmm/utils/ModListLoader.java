package com.gerusz.rwmm.utils;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gerusz.rwmm.ModManagerConfig;
import com.gerusz.rwmm.RwmmApplication;
import com.gerusz.rwmm.data.ActiveModList;
import com.gerusz.rwmm.data.SavegameList;

import java.io.File;
import java.io.IOException;

public class ModListLoader {

	private static final XmlMapper mapper = new XmlMapper();

	public static ActiveModList loadFromModList() {
		ModManagerConfig config = RwmmApplication.modManagerConfig;
		File modListFile = new File(config.getModListPath());
		try {
			return mapper.readValue(modListFile, ActiveModList.class);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-3);
			return null;
		}
	}

	public static ActiveModList loadFromSavefile(SavegameList.Savegame savegame) {
		return new ActiveModList(savegame.getModFolders(), savegame.getGameVersion());
	}
}
